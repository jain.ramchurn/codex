---
title: "Awesome Resources"
date: 2022-08-01T21:19:04+04:00
draft: False
---

* [Container training](https://container.training/)
* [Jérôme Petazzoni's training courses](https://tinyshellscript.com/training.html)
* [Learning Containers, Kubernetes, and Backend Development with Ivan Velichko](https://iximiuz.com/en/)
