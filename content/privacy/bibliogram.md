---
title: "Bibliogram"
date: 2022-08-01T20:12:19+04:00
draft: true
---

[Bibliogram](https://github.com/Booteille/bibliogram) is an alternative front-end for Instragram and it works without client-side Javascript and requires no sign-up.

However, Instagram has been applying restrictive limits to requests from IPs that are detected as cloud-servers.
[Future of biblogram](https://proxy.vulpes.one/gemini/cadence.moe/gemlog/2020-12-17-future-of-bibliogram.bliz) is a post which goes into details about the situation and possible solutions.
