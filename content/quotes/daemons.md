---
title: "Da3mons"
date: 2022-07-26T15:17:26+04:00
draft: false
---
There's a saying - 'The devil is at his strongest while we're looking the other way.' 
Like a program running in the background silently. While we're busy doing other shit. `Daemons,` they call them.They perfom action without user interaction. Monitoring, logging, notifications, primal urges, repressed memories, unconscious habits. They're always there, always active. You can try to be right, you can try to be good, youcan try to make a difference. But it's all bullshit. Cause intentions are irrelevant. They don't drive usm daemons do. And me? I've got more than most.

> Mr. Robot Season 1 Episode 4

