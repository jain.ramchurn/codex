---
title: "Serial Terminal"
date: 2022-07-26T15:17:16+04:00
draft: False
---

# Serial terminal

## Configuration on the host

1. Add a virtual serial port to the VM

```bash
qm set <VMID> -serial0 socket
```

(or)

```bash
echo "serial0: socket" >> /etc/pve/qemu-server/<VMID>.conf
```

## Configuration on the guest

### Configure the terminal

1. Enable a serial console

```bash
systemctl enable serial-getty@ttyS0.service
systemctl start serial-getty@ttyS0.service
```

2. Instruct grub2 to send the boot messages on the serial port by editing `/etc/default/grub` and adding:

```bash
GRUB_CMDLINE_LINUX="quiet console=tty0 console=ttyS0,115200"
```

3. run `update-grub` afterward.

### Allow password-less root login on the serial console

_TIPS_

Add in .bashrc
```
export SYSTEMD_EDITOR=vim
```

1. Run `systemctl edit serial-getty@ttyS0.service`, and add the following:

```
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -- \\u' --keep-baud 115200,38400,9600 --noclear --autologin root ttyS0 $TERM
```

This will cause `agetty` to auto-login the `root` user, but with only this change the system will still prompt you for the root password.

```
-o  --login-option
-p  --login-pause
\u  user
-J --noclear (Do not clear the screen before prompting the login name. By deafult the screen is cleared)

```

2.  We can configure `/etc/pam.d/login` to authenticate `root` logins on the console without a password.  Add the following to the top of `/etc/pam.d/login`:

   ```
   auth sufficient pam_listfile.so item=tty sense=allow file=/etc/securetty onerr=fail apply=root
   ```

   This will cause the PAM stack to check for the login tty in `/etc/securetty`, and to skip other authentication mechanisms if it finds it.



3.  Add the serial port to `/etc/securetty`.

   ```
   echo ttyS0 >> /etc/securetty
   ```

### Connecting to the Serial Terminal

On the host, just enter

```bash
qm terminal <VMiD>
```

and enter **enter**  a second time to  get the prompt.

**NOTE**:  if it seems this is not working, and if you have defined ttyS1, you can connect to it with the command:

```
qm terminal <VMiD> -iface serial1
```

Remember the `qm terminal` uses Ctrl-O as shortcut, so saving a file from `nano` with Ctrl-O will log you out instead.



```bash
root@zain-lab ~ # qm terminal 101011
starting serial terminal on interface serial0 (press Ctrl+O to exit)

root@console ~ #
root@console ~ # logout

Debian GNU/Linux 10 console ttyS0

console login: root (automatic login)

Last login: Thu May  6 18:14:49 UTC 2021 on ttyS0
Linux console 4.19.0-10-amd64 #1 SMP Debian 4.19.132-1 (2020-07-24) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
root@console ~ #
root@console ~ # root@zain-lab ~ #

```





https://notesofaprogrammer.blogspot.com/2020/05/enabling-serial-console-on-debian-linux.html

https://unix.stackexchange.com/questions/552576/allow-passwordless-root-login-on-the-serial-console

http://www.linux-pam.org/Linux-PAM-html/sag-pam_listfile.html

https://wiki.archlinux.org/title/Systemd#Change_default_target_to_boot_into

https://www.debuntu.org/how-to-set-up-a-serial-console-on-debian/

