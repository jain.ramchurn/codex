---
title: "Kernel Newbies"
date: 2022-08-10T20:49:06+04:00
draft: False
---

TIL about [kernelnewbies](https://kernelnewbies.org/). It is a community of aspiring Linux kernel developers who work to improve their Kernels and more experienced developers willing to share their knowledge. 

You can  follow a tutorial for creating your first kernel patch, or read about Linux kernel Hacking. You can further learn about kernel subsystem as well.

