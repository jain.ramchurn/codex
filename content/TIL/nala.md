---
title: "Nala"
date: 2022-08-02T20:57:11+04:00
draft: False
---

TIL about [Nala](https://gitlab.com/volian/nala), a front-end for libapt-pkg. It is written in Python and uses the `python-apt` api.

## Installation

Nala is the testing  and sid repos for Debian.
So a simple apt is sufficed.

### Installation via Repository

```
echo "deb https://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null
```

More details [here](https://gitlab.com/volian/nala/-/wikis/Installation)


## Features

* Parrallel downloads - downloads 3 packages  per unique mirror by defaults
* Fetch - get the fastest mirror
* History - print summary of transactions  and manipulate history with `nala history undo <ID>` or `nala history redo <ID>`


