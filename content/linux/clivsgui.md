---
title: "CLI vs GUI"
date: 2022-07-26T15:18:47+04:00
draft: False
---

> In the Beginning.. Was the Command Line. - Neal Stephenson.

A dark screen with a blinking cursor.
With a few keystrokes, you can hack the world.


On the command line, you must know what you are doing.
With a GUI, you can guess. You can guess a lot, actually, and just poke around all you want because most GUIs come with an undo feature.

There is no "undo" on the command line.

Command line is like real life. There is no undo button in real life.

GUIs have made us lazy - lazy at thinking, lazy at figuring things out. Just do it: if you don't like it, just Ctrl-Z. Just throw that document away and leave it in the recycle bin. If you decide you want/need it later, you can just drag it on out of there.

With a GUI, that "undo" button is always an option (except for the rare occasion when it is not. Those times are fun).  But in real life, you can't `unmake` a mistake.

The command line makes you think. It makes you plan, it makes you think about the end goal, it makes you remember past failurees.
The command line makes you think about `outcomes`.

