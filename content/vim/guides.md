---
title: "Guides & resources to learn Vim"
date: 2022-08-01T17:28:30+04:00
draft: False
---

* [At least one Vim trick you might not know](https://www.hillelwayne.com/post/intermediate-vim/)
* [A Vim Guide for Advanced Users](https://thevaluable.dev/vim-advanced/)
* [Vim Tips for the Intermediate Vim User](https://jemma.dev/blog/intermediate-vim-tips)

