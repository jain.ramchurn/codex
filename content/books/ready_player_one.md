---
title: "Ready_player_one"
date: 2022-07-26T15:16:18+04:00
draft: False
---

"Here's the deal, Wade. You're something called a 'human being'. That's
a really smart kind of animal. Like very other animal on this planet,
we descended from a single-celled organism that lived millions of years
ago. This happened by a process called  evolution, and you'll learn more
about it later. But trust me, that's really how we all got here. There's proof
of it everywhere, buried in the rocks. That story you heard? About how
we were all created by a super-powerful dude named God who lives up in
the sky? Total bullshit. The whole God thing is actually an ancient fairy
tale that people have been telling one another for thousands of years. We
made it all up. Like Santa Claus and the Easter Bunny.
"Oh, and by the way ... there's no Santa Claus or Easter Bunny. Also
bullshit. Sorry, kid. Deal with it.

"You're probably wondering what happened before you got here. An awful
lot of stuff, actually. Once we evolved into humans, things got pretty
interesting. We figured out how to grow food and domesticate animals
so we didn't have to spend all of our time hunting. Our tribes got much
bigger, and we spread across the entire planet like an unstoppable virus.
Then, after fighting a bunch of wars with each other over land, resources,
 and our made-up gods, we eventually got all of our tribes organized into a
'global civilization'. But, honestly, it wasn't all that organized, or
civilized, and we continue to fight a lot of wars with each other. But we
also figured out how to do science, which helped us develop technology.
For a bunch of hairless apes, we've actually managed to invent some incredible
things. Computers. Medicine. Lasers. Microwave ovens. Artificial hearts. Atomic bombs.
We even sent a few guys to the moon and brought them back.
We also created a  global communication network that lets us all talk to each other,
 all around the world, all the time. Pretty impressive, right?

"But that's where the bad news comes in. Our global civilization came at a huge cost.
We needed a whole bunch of energy to build it, and we got that energy by burning fossil fuels,
which came from dead plants and animals buried deep in the ground.
We used up most of this fuel before it got here, and now it's pretty much gone.
This means that we no longer have enough energy to keep our civilization running like it was before.
So we've had to cut back.
Big-time. We call this the Global Energy Crisis, and it's been going on for a while now.

"Also, it turns out that burning all of those fossil fuels had some nasty side effects,
like raising the temperature of our planet and screwing up the
environment. So now the polar ice caps are melting, sea level are rising,
and the weather is all messed up. Plants and animals are dying off in record numbers,
and lots of people are starving and homeless.
And we're still fighting wars with each others, mostly over the few resources we have left.

"Bassically, kid, what this all means is that life is a lot tougher than it
used to be, in the Good Old Days, back before you were born, Things used to be awesome,
but now they're kinda terrifying. To be onest, the future doesn't look too bright.
You were born at a pretty crappy time in history. And it looks like things are only gonna
get worse from here on out. Human civilization is in 'decline'. Some people even say it's 'collapsing'.

"You're probably wondering what's going to happen to you. That's easy.
The same thing is going to happen to you that has happened to every
other human being who has ever lived. You're going to die. We all die.
That's just how it is.

"What happens when you die? Well, we're not completely sure. But
the evidence seems to suggest that nothing happens.
You're just dead, your brain stop working, and then you're not around
to ask annoying  questions anymore.
Those stories you heard?  About going to a wonderful place called 'heaven' where
there's no more pain or death, and you live forever in a state of
perpetual happiness? Also total bullshit. Just like all that God Stuff?
There's no evidence of a heaven, and there never was. We made that up too.
Wishful thinking.
So now you have to live the rest of your life knowing you're going to die someday and disappear forever.

 "Sorry."

- 0001
Ready Player One

